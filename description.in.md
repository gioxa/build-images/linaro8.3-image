
[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

# ${ODAGRUN_IMAGE_REFNAME}

${ODAGRUN_IMAGE_TITLE}
 

Custom build image for crosscompiling Aarch64 GNU GCC8.2 for mainline u-boot/kernel compilation.

## with `make_os.conf`:

```bash

${MAKE_OS_CONFIG}

```
## Toolchain

from: [Download aarch64 tc](https://developer.arm.com/-/media/Files/downloads/gnu-a/8.2-2019.01/gcc-arm-8.2-2019.01-x86_64-aarch64-linux-gnu.tar.xz?revision=21270570-4ec0-4bad-a9e1-09707614066a&la=en)

## extra

with [trap_print](https://gitlab.com/gioxa/trap_print)

## with `docker_config.yml`:

```yaml

${DOCKER_CONFIG_YML}

```
 
 ---

*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter from GitLab-CI*